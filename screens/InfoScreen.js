import React from 'react';
import {
  Image,
  Linking,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {Text, Divider} from 'react-native-elements'

import { MonoText } from '../components/StyledText';
import ConnectionsListView from '../views/ConnectionsListView'

export default class InfoScreen extends React.Component {
  static navigationOptions = {
    title: 'Info',
  };

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../assets/icons/app-icon.png')}
          style={styles.appIcon}
        />
        <Text h4>RailsFox</Text>
        <Divider style={{ backgroundColor: 'blue' }} />
        <Text style={styles.infoHeader} h5>Wersja</Text>
        <Text style={styles.infoContent}>v.0.1</Text>
        <Text style={styles.infoHeader} h5>Autor</Text>
        <Text style={styles.infoContent}>Konrad Strojny</Text>
        <Text style={styles.infoHeader} h5>WWW</Text>
        <Text style={styles.infoContent}>-</Text>
        <Text style={styles.infoHeader} h5>Kontakt</Text>
        <Text style={styles.infoContent}>kocasp@gmail.com</Text>
        <Text style={styles.infoHeader} h5>Opis</Text>
        <Text style={styles.infoContent}>Aplikacja sledzi i wyswietla ceny polaczen pociagow Express Intercity Pendolino</Text>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  appIcon: {
    width: 100,
    height: 120,
    resizeMode: 'contain',
  },
  infoHeader: {
    marginTop: 10,
  },
  infoContent: {
    textAlign: 'center',
    color: 'rgba(0,0,0,0.5)',
  }
});
