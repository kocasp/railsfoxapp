import React from 'react'
import {
  Linking,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import axios from 'axios';
import 'intl';
import 'intl/locale-data/jsonp/pl';

var groupBy = require("group-by");

class CourseRow extends React.Component {

  constructor(props){
    super(props)
    this.handleClick = this.handleClick.bind(this)
  };

  handleClick(e){
    // this.props.navigator.navigate('Connection', {id: this.props.id, name: this.props.name})
    alert("RailsFox jedynie wyswietla kursy i ich ceny. Aby kupic bilet na wybranie polaczenie, wejdz na strone intercity.pl")
  };

  render(){
    const date_formatter = new Intl.DateTimeFormat( 'pl' )
    const weekday_formatter = new Intl.DateTimeFormat( 'pl' , {weekday: 'long'})
    const time_formatter = new Intl.DateTimeFormat( 'pl' , { hour: 'numeric', minute: 'numeric'})
    const departure_date = new Date(this.props.departure_time)
    const arrival_date = new Date(this.props.arrival_time)

    // const { navigate } = this.props.navigation;
    return(
      <TouchableOpacity
        style={styles.optionsContainer}
        onPress={this.handleClick}>
        <View style={styles.option}>
          <View style={styles.optionIconContainer}>
          </View>
          <View style={{alignSelf: 'stretch',}}>
            <Text style={styles.courseDate}>
              {date_formatter.format(departure_date)} - {weekday_formatter.format(departure_date)}
            </Text>
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={styles.stationName}>
                {this.props.connection.station.name}:
              </Text>
              <Text style={styles.optionText}>
                {time_formatter.format(departure_date)}
              </Text>
            </View>
            <View>
              <Text style={styles.stationName}>
                {this.props.connection.connected_station.name}:
              </Text>
              <Text style={styles.optionText}>
                {time_formatter.format(arrival_date)}
              </Text>
            </View>
            <View style={styles.priceColumn}>
              <Text style={styles.cheapInfo}>cena:</Text>
              <Text>{this.props.price}</Text>
            </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
}

export default class CoursesListView extends React.Component {
  state = {courses: []}

  constructor(props){
    super(props)
    this.groupCourses = this.groupCourses.bind(this)
  };

  componentWillMount(){
    // axios.get("http://www.mocky.io/v2/59922fa11000007f0a557723")
    axios.get("https://private-77d045-railsfox.apiary-mock.com/v1/connection/1")
      .then(response => this.setState({courses: response.data}));
  }

  groupCourses(courses_array){
    date_formatter = new Intl.DateTimeFormat( 'pl' )
    weekday_formatter = new Intl.DateTimeFormat( 'pl' , {weekday: 'long'})
    courses_array.map(function(el){el["parsed_date"]=date_formatter.format(new Date(el["departure_time"])); return el})
    grouped_courses = groupBy(courses_array, 'parsed_date');
    z = Object.keys(grouped_courses)
    formed_courses = z.map(function(date){a = []; a[0] = date; a[1] = grouped_courses[date]; return a})
    return formed_courses
  }
// (8) [Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2), Array(2)]
// (2) ["21.07.2017", Array(7)]
// (2) ["22.07.2017", Array(1)]
// (2) ["23.07.2017", Array(1)]
// (2) ["24.07.2017", Array(1)]
// (2) ["25.07.2017", Array(1)]
// (2) ["26.07.2017", Array(1)]
// (2) ["27.07.2017", Array(1)]
// (2) ["28.07.2017", Array(1)]
  render() {
    if (this.state.courses.length > 0){
      formed_courses = this.groupCourses(this.state.courses)
      return (
        <View style={styles.container}>
          <ScrollView
            style={styles.container}>

            {formed_courses.map(
              course => (
                <View key={course[0]}>
                  <Text style={styles.dateHeader}>{course[0]} </Text>
                  {course[1].map(
                    details => <CourseRow connection={this.props.connection} key={details.id} departure_time={details.departure_time} arrival_time={details.arrival_time} id={details.id} price={details.price}/>

                    )}
                </View>
                )
              )}

          </ScrollView>
        </View>
      );
    } else {
      return (
        <View>
          <ActivityIndicator />
          <Text style={styles.loading_text}>Pobieranie kursów</Text>
        </View>
      );
    }
  }
}


const styles = StyleSheet.create({
  dateHeader: {
    backgroundColor: "#052e39",
    color: "#fff",
    paddingVertical: 5,
    textAlign: 'center',
  },
  loading_text:{
    textAlign: 'center',
    fontSize: 10,
  },
  container: {
    flex: 1,
    alignSelf: 'stretch',
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12,
  },
  optionsContainer: {
    borderBottomWidth: 2,
    borderBottomColor: '#EDEDED',
  },
  optionIconContainer: {
    marginRight: 9,
    alignSelf: 'stretch',
  },
  option: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    alignSelf: 'stretch',
    justifyContent: 'center',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
  courseDate: {
    fontSize: 10,
    color: '#555',
  },
  stationName: {
    fontSize: 10,
  },
  cheapInfo: {
    fontSize: 10,
  },
  priceColumn: {
    alignItems: 'flex-end',
  }
});
